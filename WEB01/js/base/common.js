

class CommonJS{
    /**
     * Định dạng : Hiển thị thông tin ngày là (ngày/tháng/năm)
     * @param {Date} date 
     * Author : LEQUAN ()
     */  
    static formatDateDDMMYY(date){
    
        if(date){
            const newDate = new Date(date);
            let day = newDate.getDate();
            day = day < 10 ? `0${day}` : day;
            let month = newDate.getMonth();
            month = month < 10 ? `0${month}` : month;
            let year = newDate.getFullYear();
            return `${day}/${month}/${year}`;
        }
        else{
            return "";
        }
    }
}