

$(document).ready(function() {
    formMode = null;
    employeeIdSelected = null;
    employeeIdDelete = null;
    loadData();
    $('.m-toast-msg').hide();
    // $('.m-dialog-modal').show();
    loadDepartment();
    $('.m-btn').click(clickAddEmployee);
    $('.m-popup-header-close').click(clickCloseEmployee);
    $('.m-popup-footer-btn-left').click(clickCloseEmployee);
    $('.m-reload').click(loadData);
    $('.m-delete').click(deleteData);

    $('.m-popup-footer-btn-right').click(function() {
        // Thu thập thông tin đã nhập liệu
        const employeecode = $('#employeecode').val();
        const fullname = $('#name').val();
        const dateofbirth = $('#dateofbirth').val();
        const sex = $('#sex').data('value');
        const email = $('#email').val();
        const phonenumber = $('#phonenumber').val();
        const address = $('#address').val();
        const department = $('#div-department').data('value');
        const salary = $('#salary').val();
      // tao object chứa dữ liệu
      let employee = {
         "EmployeeCode": employeecode,
         "FullName": fullname,
         "Gender": sex,
         "DateOfBirth": dateofbirth,
         "PhoneNumber": phonenumber,
         "Email": email,
         "Address": address,
         "Salary": salary,
         "DepartmentId": department
      };
      if(formMode =='add'){
        // sử dụng ajax post dữ liệu lên server
        $.ajax({
            type: "POST",
            url:"http://cukcuk.manhnv.net/api/v1/Employees",
            data: JSON.stringify(employee),
            dataType: "json",
            contentType: "application/json",
            success: function (response) {
                loadData();
            }
        }); 
      }
      else {
        $.ajax({
            type: "PUT",
            url: `http://cukcuk.manhnv.net/api/v1/Employees/${employeeIdSelected}`,
            data: JSON.stringify(employee),
            dataType: "json",
            contentType: "application/json",
            success: function (response) {
                console.log(response);
            }
        }); 
      }
      loadData();
      $('.m-dialog-modal').hide();
    })

    // Sự kiện khi nhấn đúp chuột vào dòng dữ liệu trong table => Hiển thị form chi tiết nhân viên
    $('table#tblEmployee').on('dblclick', 'tbody tr',rowOnDbClick);
    $('table#tblEmployee').on('click', 'tbody tr',rowDeleteOnClick);
    // Gán background cho dòng dữ liệu bị xóa
    function rowDeleteOnClick(){
        $(this).siblings().removeClass('row-selected');
        $(this).addClass('row-selected');
         // Lấy Id của nhân viên tương ứng:
        employeeIdDelete = $(this).data('employeeId');
    }
    // hàm xóa dữ liệu
    function deleteData(){
        // xóa dữ liệu nhân viên vừa chọn
        $.ajax({
            type: "DELETE",
            url: `http://cukcuk.manhnv.net/api/v1/Employees/${employeeIdDelete}`,
            success: function (response) {
                loadData();
                $('.m-toast-msg').show();
                setTimeout(function () {
                    $('.m-toast-msg').hide();
                },2000);
            }
        });
    }


    function rowOnDbClick(sender) {
        formMode = 'edit';
        // Lấy Id của nhân viên tương ứng:
        var employeeId = $(this).data('employeeId');
        employeeIdSelected = employeeId;
        // lấy thông tin chi tiết của nhân viên
        $.ajax({
            type: "GET",
            url: `http://cukcuk.manhnv.net/api/v1/Employees/${employeeId}`,
            success: function (emp) {
                // biding dữ liệu lên form chi tiết tương ứng với từng thông tin đối tượng:
                $('#employeecode').val(emp.EmployeeCode);
                $('#name').val(emp.FullName);
                $('#dateofbirth').val(emp.dateOfBirth);
                // $('#sex').data('value');
                $('#email').val(emp.email);
                $('#phonenumber').val(emp.phonenumber);
                $('#address').val(emp.Address);
                // $('#div-department').data('value');
                $('#salary').val(emp.Salary);
                 // Hiển thị form thông tin chi tiết
                 $('.m-dialog-modal').show();
            }
        });
    }

 
    // load dữ liệu phòng ban
    function loadDepartment(){
        // lấy dữ liệu về
        $.ajax({
            type: "GET",
            url: "http://cukcuk.manhnv.net/api/v1/Departments",
            success: function (response) {
                for(const department of response){
                    // let HTML = `<option value="${department.DepartmentId}">${department.DepartmentName}</option>`
                    let optionHTML = `<div class="mcombobox-item" value='${department.DepartmentId}'>${department.DepartmentName}</div>`
                    $('#mcombobox-data').append(optionHTML);
                    let itemDataElements = $('#div-department').find('.mcombobox-data').html();
                    $('#div-department').data('itemDataElement',itemDataElements);
                }
            }
        });
    }
    


})

// hàm hiển thị popup
function clickAddEmployee(){
    formMode = 'add';
  $('.m-dialog-modal').show();
    // Lấy mã nhân viên mới và hiển thị lên ô nhập mã nhân viên
    $.ajax({
        type: "GET",
        url: "http://cukcuk.manhnv.net/api/v1/Employees/NewEmployeeCode",
        success: function (response) {
            $('#employeecode').val(response);
            // focus vào ô nhập liệu đầu tiên
            $('#employeecode').focus();
        }
    });
}

// hàm thoat popup
function clickCloseEmployee(){
    $('.m-dialog-modal').hide();
    $('.m-input-form').val('');
    $('.m-input-address').val('');
}

// hàm show loading
function showLoading() {
    $('.m-loading').show();
}

// hàm load dữ liệu
function loadData() {
    // làm trống dữ liệu
    $('.m-loading').show();
    $('.m-table tbody').empty();
    let employees = [];
    $.ajax({
        type: "GET",
        url: "http://cukcuk.manhnv.net/api/v1/Employees",
        // data: "data",
        // dataType: "json",
        // contentType: "application/json",
        async: false,
        success: function(response) {
            employees = response;
        },
        error: function(response) {
            alert('Có lỗi xảy ra');
        }
    });

    let employeetable = $('.m-table tbody');
    $.each(employees, function(index, employee) {
        let code = employee.EmployeeCode;
        let fullName = employee.FullName;
        let gender = employee.GenderName;
        let dateOfBirth = employee.DateOfBirth;
        let department = employee.DepartmentName;
        let salary = employee.Salary;
        salary = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(salary)

        // xử lí định dạng dữ liệu
        // định dạng ngày sinh: Phải có dạng hiển thị là Ngày/Tháng/Năm
        dateOfBirth = new Date(dateOfBirth);
        let date = dateOfBirth.getDate();
        date = date < 10 ? `0${date}` : date;
        let month = dateOfBirth.getMonth();
        month = month < 10 ? `0${month}` : month;
        let year = dateOfBirth.getFullYear();
        dateOfBirth = `${date}/${month}/${year}`;

        let tr = $(`
                <tr>
                    <td class="text-align-left">${code}</td>
                    <td class="text-align-left">${fullName}</td>
                    <td class="text-align-left">${gender}</td>
                    <td class="text-align-center">${dateOfBirth}</td>
                    <td class="text-align-left">${department}</td>
                    <td class="text-align-right">${salary}</td>
                </tr>       
                 `);
            tr.data('employeeId',employee.EmployeeId);             
             employeetable.append(tr);
        $('.m-loading').hide();
    })
}