$(document).ready(function(){
    EmployeePages = new EmployeePage();
})

class EmployeePage {
 
    formMode = null;
    employeeIdSelected = null;
    employeeIdDelete = null;
    constructor(){
        // TitlePage = "Danh sách khách hàng"
        // this.formMode = '1';
        this.loadData();
        this.loadDepartment();
        this.innitEvent();
    }
   
    // thực hiện load dữ liệu
    loadData(){
         // làm trống dữ liệu
         $('.m-toast-msg').hide();
        $('.m-loading').show();
        $('.m-table tbody').empty();
        let employees = [];
        // Gọi api thực hiện lấy dữ liệu về -> sử dụng ajax
        
        $.ajax({
            type: "GET",
            url: "http://cukcuk.manhnv.net/api/v1/Employees",
            success: function (response) {
                if(response){
                    let employees = response;
                    // build từng tr và append vào table to
                            
            let employeetable = $('.m-table tbody');
            $.each(employees, function(index, employee) {
                let code = employee.EmployeeCode;
                let fullName = employee.FullName;
                let gender = employee.GenderName;
                let dateOfBirth = employee.DateOfBirth;
                let department = employee.DepartmentName;
                let salary = employee.Salary;
                salary = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(salary)
                // build dữ liệu hiển thị lên table
                let tr = $(`
                        <tr>
                            <td class="text-align-left">${code}</td>
                            <td class="text-align-left">${fullName}</td>
                            <td class="text-align-left">${gender}</td>
                            <td class="text-align-center">${CommonJS.formatDateDDMMYY(dateOfBirth)}</td>
                            <td class="text-align-left">${department}</td>
                            <td class="text-align-right">${salary}</td>
                        </tr>       
                        `);
                    tr.data('employeeId',employee.EmployeeId);             
                    employeetable.append(tr);
                $('.m-loading').hide();
    })
                }
            }
        });

       
    }

    // Gán sự kiện cho các thành phần có trong trang
    innitEvent(){
        // button Add 
        $('.m-btn').click(this.clickAddEmployee.bind(this));
        // button saveData
        $('.m-popup-footer-btn-right').click(this.saveData.bind(this));
        // row onclick
        $('table#tblEmployee').on('click', 'tbody tr',this.rowDeleteOnClick.bind(this));
        // row ondouble
        // Sự kiện khi nhấn đúp chuột vào dòng dữ liệu trong table => Hiển thị form chi tiết nhân viên
        $('table#tblEmployee').on('dblclick', 'tbody tr',this.rowOnDbClick.bind(this));
        // button close
        $('.m-popup-header-close').click(this.clickCloseEmployee);
        $('.m-popup-footer-btn-left').click(this.clickCloseEmployee);
        // button delete
        $('.m-delete').click(this.delete.bind(this));
        // button reload
        $('.m-reload').click(this.loadData);
    }


    // hiển thị popup
    clickAddEmployee(){
         this.formMode = 'add';
      $('.m-dialog-modal').show();
      
        // Lấy mã nhân viên mới và hiển thị lên ô nhập mã nhân viên
        $.ajax({
            type: "GET",
            url: "http://cukcuk.manhnv.net/api/v1/Employees/NewEmployeeCode",
            success: function (response) {
                $('#employeecode').val(response);
                // focus vào ô nhập liệu đầu tiên
                $('#employeecode').focus();
            }
        });
    }
    
    
    // thoat popup
    clickCloseEmployee(){
        $('.m-dialog-modal').hide();
        $('.m-input-form').val('');
        $('.m-input-address').val('');
    }

    // hàm load dữ liệu lên form
    rowOnDbClick(sender) {
        this.formMode = 'edit';
        // Lấy Id của nhân viên tương ứng:
        let currentRow = sender.currentTarget;
        var employeeId = $(currentRow).data('employeeId');
        this.employeeIdSelected = employeeId;
       
        // lấy thông tin chi tiết của nhân viên
        $.ajax({
            type: "GET",
            url: `http://cukcuk.manhnv.net/api/v1/Employees/${employeeId}`,
            success: function (emp) {
                // biding dữ liệu lên form chi tiết tương ứng với từng thông tin đối tượng:
                $('#employeecode').val(emp.EmployeeCode);
                $('#name').val(emp.FullName);
                $('#dateofbirth').val(emp.dateOfBirth);
                // $('#sex').data('value');
                $('#email').val(emp.email);
                $('#phonenumber').val(emp.phonenumber);
                $('#address').val(emp.Address);
                // $('#div-department').data('value');
                $('#salary').val(emp.Salary);
                 // Hiển thị form thông tin chi tiết
                 $('.m-dialog-modal').show();
                 
            }
        });
    }

    // hàm lưu dữ liệu
    saveData(){
        var me = this;
            // Thu thập thông tin đã nhập liệu
            const employeecode = $('#employeecode').val();
            const fullname = $('#name').val();
            const dateofbirth = $('#dateofbirth').val();
            const sex = $('#sex').data('value');
            const email = $('#email').val();
            const phonenumber = $('#phonenumber').val();
            const address = $('#address').val();
            const department = $('#div-department').data('value');
            const salary = $('#salary').val();
          // tao object chứa dữ liệu
          let employee = {
             "EmployeeCode": employeecode,
             "FullName": fullname,
             "Gender": sex,
             "DateOfBirth": dateofbirth,
             "PhoneNumber": phonenumber,
             "Email": email,
             "Address": address,
             "Salary": salary,
             "DepartmentId": department
          };
          if(this.formMode =='add'){
            // sử dụng ajax post dữ liệu lên server
            $.ajax({
                type: "POST",
                url:"http://cukcuk.manhnv.net/api/v1/Employees",
                data: JSON.stringify(employee),
                dataType: "json",
                contentType: "application/json",
                success: function (response) {
                    me.loadData();
                }
            }); 
          }
          else {
            $.ajax({
                type: "PUT",
                url: `http://cukcuk.manhnv.net/api/v1/Employees/${this.employeeIdSelected}`,
                data: JSON.stringify(employee),
                dataType: "json",
                contentType: "application/json",
                success: function (response) {
                    console.log(response);
                }
            }); 
          }
        this.loadData();
        $('.m-dialog-modal').hide();
    }

    
    // load dữ liệu phòng ban
    loadDepartment(){
        // lấy dữ liệu về
        $.ajax({
            type: "GET",
            url: "http://cukcuk.manhnv.net/api/v1/Departments",
            success: function (response) {
                for(const department of response){
                    // let HTML = `<option value="${department.DepartmentId}">${department.DepartmentName}</option>`
                    let optionHTML = `<div class="mcombobox-item" value='${department.DepartmentId}'>${department.DepartmentName}</div>`
                    $('#mcombobox-data').append(optionHTML);
                    let itemDataElements = $('#div-department').find('.mcombobox-data').html();
                    $('#div-department').data('itemDataElement',itemDataElements);
                }
            }
        });
    }
    
     // Gán background cho dòng dữ liệu bị xóa
    rowDeleteOnClick(sender){
        let currentRow = sender.currentTarget;
        $(currentRow).siblings().removeClass('row-selected');
        $(currentRow).addClass('row-selected');
         // Lấy Id của nhân viên tương ứng:
        this.employeeIdDelete = $(currentRow).data('employeeId');
    }

    delete(){
 // xóa dữ liệu nhân viên vừa chọn
        var me = this;
    $.ajax({
        type: "DELETE",
        url: `http://cukcuk.manhnv.net/api/v1/Employees/${this.employeeIdDelete}`,
        success: function (response) {
            me.loadData();
            $('.m-toast-msg').show();
            setTimeout(function () {
                $('.m-toast-msg').hide();
            },2000);
        }
    });
    
    }
}