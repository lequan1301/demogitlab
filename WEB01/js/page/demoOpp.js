

class Person {
    Name;
    Age;
    constructor() {

    }
    getName() {

    }
}




// định nghĩa 1 class trong js
// Đối với việc kế thừa thì bắt buộc trong hàm khởi tạo phải sử dụng lệnh super();
class Employee extends Person { 
    // trường thuộc tính ko cần phải khai báo kiểu dữ liệu
    FullName ;
    GenderName = 'Nam';
    static Address;
    // Hàm khởi tạo có thể có tham số hoặc không
    // Khi khởi tạo có thể truyền hoặc không truyền đối số đều đc
    constructor(name){
        super();
        // set các giá trị cho các thành phần trong class
        this.FullName = name;
        // có thể gán giá trị cho các thuộc tính không đc khai báo trước
        this.SchoolName = 'HVKTQS';
    }

    // khai báo 1 phương thức
    // Author: Lê Quân
    getName(){
        return this.FullName;
    }

    // khai báo phương thức tĩnh
    static getSchoolName(){
        return 'MISA';
    }
}
var employee = new Employee('Lê Đoàn Anh Quân');