$(document).ready(function() {
    $('.m-combobox-btn-icon').click(btnClick);
    // $('.mcombobox-item').click(itemClick);
    //  $('.m-combobox-form').keydown(keydownInput);
    //  $('.m-combobox-form').keyup(keyUpInput);
     $('.mcombobox-data').on('click','.mcombobox-item',itemClick);
      // lưu trữ thông tin comboboxdata
      let comboboxs = $('.mcombobox');
      for (const combobox of comboboxs) {
          let itemDataElements = $(combobox).find('.mcombobox-data').html();
          $(combobox).data('itemDataElement',itemDataElements);
        //   $(combobox).find('.mcombobox-data').empty();
      }
})

function btnClick(){
 let comboboxdata = $(this).siblings('.mcombobox-data');
 comboboxdata.toggle();
}
function keyUpInput(){   
     
            switch(event.keyCode){
                case 13: 
                case 40:
                case 38:
                    break;
                default:
                    $(this).siblings('.mcombobox-data').empty();
                    let itemDataElement = $(this.parentElement).data('itemDataElement');
                    console.log(this)
                    // build html cho các combobox data item data
                    $(this).siblings('.mcombobox-data').html(itemDataElement);
                    // thực hiện lọc dữ liệu trong combobox data item item
                    // 1. Lấy value đã nhập trên input
                    const valueInput = this.value;
                    // 2. Duyệt từng item và thực hiện kiểm tra xem element có text trùng với value đã nhập
                    // lấy tất cả item element của combobox
                    let items = $(this).siblings('.mcombobox-data').children();
           
                    for(const item of items){
                        let text = item.textContent;
                        if(!text.toLowerCase().includes(valueInput.toLowerCase())){
                            item.remove();
                        }
                    }
                    $(this).siblings('.mcombobox-data').show();
                    break;
            }

            
}

function keydownInput(){
    let comboboxdata = $(this).siblings('.mcombobox-data');
     // lấy các item
     let items = comboboxdata.children();
    
     //kiểm tra xem có item nào đã được hover chưa
     let itemhoverred = items.filter('.m-combobox-item-hover');
     // bỏ hover tất cả các item đã đc set
    //  $(items).removeClass('m-combobox-item-hover');
     
   switch(event.keyCode){
       case 40: // nhấn phím keydown
            comboboxdata.show();
            // nếu có item đc hover trước đó thì hover tới item kế tiếp
            if(itemhoverred.length > 0){
                // lấy element kế tiếp
                let nextElement = itemhoverred.next();
                // thêm class hover cho item kế tiếp
                nextElement.addClass('m-combobox-item-hover');
                // xóa class hover của item hiện tại
                itemhoverred.removeClass('m-combobox-item-hover')
            }
            else{
                // nếu chưa có thì hover vào item đầu tiên
                // focus vào item đầu tiên
                    items[0].classList.add('m-combobox-item-hover');
            }
            break;
           
       case 38: // Nhấn phím keyup
            comboboxdata.show();
            if(itemhoverred.length > 0){
                // lấy element kế tiếp
                let nextElement = itemhoverred.prev();
                // thêm class hover cho item trước đó
                nextElement.addClass('m-combobox-item-hover');
                // xóa class hover của item hiện tại
                itemhoverred.removeClass('m-combobox-item-hover')
            }
            else{
                // focus vào item cuối cùng
                items[items.length-1].classList.add('m-combobox-item-hover');
            }
            break;
        case 13: // nhấn phím enter 
            comboboxdata.hide();
            // lấy text Element hiện tại đang đc hover
            const textCurrent = $(itemhoverred).text();
   
            // lấy value element
            const valueCurrent = itemhoverred[0].getAttribute('value');
            // truyền text và value vào input form

            // lấy thẻ cha của element hiện tại
            let implementparen = itemhoverred[0].parentElement;
           
            // gán text cho element cùng cấp
            $(implementparen).siblings('.m-combobox').val(textCurrent);
       
            // ẩn item đi
            $(implementparen).hide();

            // lấy element cha của item đc chọn

            let parentComboboxElement = $(this).parents('.mcombobox');

            // gán value cho input
            parentComboboxElement.data('value',valueCurrent);

            break;
        default:
            break;
   }
  
}

function itemClick(){
    const text = $(this).text();
    const value = this.getAttribute('value');
    let implementparen = this.parentElement;
    $(implementparen).siblings('.m-combobox').val(text);
    $('.mcombobox-data').hide();
    let parentComboboxElement = $(this).parents('.mcombobox');
    parentComboboxElement.data('value',value);
   
}




